# Enunciado

Faça um fork (público) do repositório https://gitlab.com/das-alexkutzke/pages-example e simule a sequência de desenvolvimento abaixo utilizando o método Git Flow:

1) Criação da branch `develop`;
2) Criação de uma nova página `noticias.html` em uma `feature-branch`;
3) Release da versão `0.1`;
4) Início da adição de uma imagem na página `index.html` em uma `feature-branch`;
5) `Hotfix`: typo na página `index.html`;
6) Conclusão da imagem na página `index.html`;
7) Início da branch de release versão `0.2`;
8) Correção de typo na branch de release;
9) Criação de um arquivo `css` para a página `index.html` em uma `feature-branch`;
10) Conclusão da release `0.2`;

Enviar o link para o repositório criado como resposta para o exercício no Moodle.

Obs.: não esqueça de utilizar o argumento `--no-ff` nos comando de `git merge` para evitar a realização de merges do tipo *fast-forward* e perder parte do histórico.
