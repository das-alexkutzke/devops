% Docker essencial
% Alex Kutzke
% Março 2022

# Primeiros passos

## Instalação

- Docker + Linux = <3

Pelo menos até o *Docker Desktop* =/

- De qualquer forma, docker pode ser instalado em qualquer plataforma;

[https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)

## CLI

- Nessa disciplina vamos utilizar apenas a linha de comando para interagir com o Docker;
- Nesse cenário, uma instalação Linux acaba sendo mais favorável.

- Você pode, então:
  - Usar seu próprio sistema e lidar com pequenas diferenças que seu terminal pode ter;
  - Criar uma VM pequena de alguma distribuição Linux;
  - Utilizar uma instância *Always Free* da Oracle Cloud;

## Oracle Cloud

- Utilizarei alguns exemplos com uma máquina virtual remota na disciplina;
- A Oracle Cloud oferece ótimas opções de VM gratuitas;

[https://www.oracle.com/br/cloud/](https://www.oracle.com/br/cloud/)

- Durante o cadastro, será necessário incluir um cartão de crédito. Porém, utilizaremos apenas funcionalidade completamente gratuitas.

## Criando uma instância (VM)

![](./img/oci_0.png)

## Criando uma instância (VM) (cont.)

![](./img/oci_1.png)

## Criando uma instância (VM) (cont.)

![](./img/oci_2.png)

## Criando uma instância (VM) (cont.)

![](./img/oci_3.png)

## Criando uma instância (VM) (cont.)

![](./img/oci_4.png)

## Criando uma instância (VM) (cont.)

![](./img/oci_5.png)

## Criando uma instância (VM) (cont.)

![](./img/oci_6.png)

## Criando uma instância (VM) (cont.)

![](./img/oci_7.png)

## Criando uma instância (VM) (cont.)

![](./img/oci_8.png)

## Criando uma instância (VM) (cont.)

![](./img/oci_9.png)

## Instalação docker Ubuntu - Oracle Cloud

Acessando a VM:

```bash
chmod 600 Downloads/ssh-key-YYYY-MM-DD.key
ssh -i Downloads/ssh-key-YYYY-MM-DD.key ubuntu@XXX.YYY.ZZZ.WWW
```

## Instalação docker Ubuntu - Oracle Cloud (cont.)

Atualizando pacotes e instalando dependências:

```bash
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg lsb-release
```

## Instalação docker Ubuntu - Oracle Cloud (cont.)

Adicionando repositório de pacotes oficiais docker:

```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo   "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
 $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

## Instalação docker Ubuntu - Oracle Cloud (cont.)

Instalando e testando:

```bash
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo docker run hello-world
sudo usermod -aG docker ubuntu
docker run hello-world
```

# Executando Containers

## Hello-world

```sh
docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
2db29710123e: Pull complete 
Digest: sha256:97a379f4f88575512824f3b352bc03cd75e239179eea0fecc38e597b2209f49a
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
...
```

## Imagens

```sh
docker image ls
REPOSITORY    TAG       IMAGE ID       CREATED        SIZE
hello-world   latest    feb5d9fea6a5   5 months ago   13.3kB
```

## Listando containers

```sh
docker container ls
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

```sh
docker container ls -a
CONTAINER ID   IMAGE         COMMAND    CREATED         STATUS                     PORTS     NAMES
771429c8c952   hello-world   "/hello"   3 minutes ago   Exited (0) 3 minutes ago             vibrant_cartwright
```

## O que containers rodam?

- Qualquer coisa :)
- Em geral, um container roda um comando em uma estrutura (imagem) específica:
  - Por exemplo, um container que roda um servidor web (apache, nginx ou outro);
- Assim, é comum containers que são iniciados e ficam rodando como um processo em segundo plano (background):
  - Para isso, podemos utilizar a opção `-d` do comando `docker run`;

## O que containers rodam? (cont.)

- Mas, por vezes, pode ser importante executarmos mais comandos dentro de um container em execução:
  - Nesse caso, podemos utilizar as opções `-t` e `-i` (ou apenas `-ti`).

## Executando comandos em um container ativo


```sh
docker container run -ti centos:7
Unable to find image 'centos:7' locally
7: Pulling from library/centos
2d473b07cdd5: Pull complete 
Digest: sha256:c73f515d06b0fa07bb18d8202035e739a494ce760aa73129f60f4bf2bd22b407
Status: Downloaded newer image for centos:7
[root@7195f2e6ce6b /]# ls
anaconda-post.log  dev  home  lib64  mnt  proc  run   srv  tmp  var
bin                etc  lib   media  opt  root  sbin  sys  usr
[root@7195f2e6ce6b /]# 
```

## Coluna COMMAND

```sh
docker ps
CONTAINER ID   IMAGE      COMMAND       CREATED         STATUS         PORTS     NAMES
f885fbeebd5d   centos:7   "/bin/bash"   7 seconds ago   Up 5 seconds             quirky_boyd
```

## Interrompendo Containers


```sh
docker container stop [CONTAINER ID]
```

Para remover um container:

```sh
docker container rm [CONTAINER ID]
```

## Status

Para verificar os recursos utilizados por um container:

```sh
docker container stats b9b33664481a
CONTAINER ID   NAME               CPU %     MEM USAGE / LIMIT     MEM %     NET I/O     BLOCK I/O     PIDS
b9b33664481a   infallible_brown   0.00%     2.676MiB / 967.2MiB   0.28%     876B / 0B   0B / 8.19kB   3
```

## Verificando processos de um container

```sh
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                8352                8329                0                   22:34               ?                   00:00:00            nginx: master process nginx -g daemon off;
systemd+            8414                8352                0                   22:34               ?                   00:00:00            nginx: worker process
systemd+            8415                8352                0                   22:34               ?                   00:00:00            nginx: worker process
```

## Acompanhando saída

Para acompanhar a saída de um container, utilize o seguinte:

```sh
docker container logs -f b9b33664481a
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
```

## Nomeando um container

É possível dar um nome a um container para facilitar a sua manipulação:

```sh
docker container run -ti --name teste debian
```

Após, podemos utilizar o nome do container para outras operações:

```sh
docker logs -f teste
```

## Limitando recursos

É possível determinar limites de **cpu** e de **memória** para um dado container:

```sh
docker container run -ti --cpus=0.5 --memory=512M --name novo_container debian
```

Para atualizar limites e outras configurações, utilize `docker container update ...`

# Dockerfile

## Dockerfile

- É um arquivo que descreve os passos para a criação de uma imagem Docker;
- Sistema muito interessante para construir imagens a partir de outras imagens;
- Sintaxe e formato próprio, porém, bastante simples.

## Definição oficial

> Dockerfile é um documento de texto que contem todos os comandos que um 
> usuário poderia digitar no terminal para criar uma imagem. Usando 
> o `docker build`, usuários podem criar builds de imagens automáticos
> que executam uma série de comandos em sucessão.

## Build

- `docker build` é um comando utilizado para criar imagens a partir de dois itens:
  - Um **dockerfile**;
  - Um **contexto**;
    - Poder ser uma pasta no disco local (`PATH`);
    - Ou um repositório git (`URL`);

- Contexto seria o conjunto de arquivos adicionais que o build deve considerar.

## Exemplo

```dockerfile
FROM debian

RUN /bin/echo "HELLO DOCKER"
```

Por padrão, o arquivo Dockerfile de um projeto se chama `Dockerfile`. :)

## Utilizando o Dockerfile

- Para utilizar o arquivo anterior, vamos, salvá-lo em uma pasta:

```sh
mkdir nova_imagem
cd noma_imagem
vim Dockerfile
```

## Utilizando o Dockerfile (cont.)

- O comando que "utiliza" um dockerfile é o `docker build`:
  - Esse comando tem por função criar uma nova imagem:

```bash
docker build --tags hello-world-debian:1.0 .
```

Atenção ao `.` no final. Ele indica que queremos criar uma imagem baseada na **pasta** atual, a qual deve conter um arquivo `Dockerfile`.

## Utilizando o Dockerfile (cont.)

- O parâmetro `--tags` fornece um nome para a **imagem**:
  - Nomes de imagem costumam ter a seguinte sintaxe: `nome:versão`.

## Instruções comuns

* **FROM**: Determina uma imagem base;
- **RUN**: Executa um comando durante o build;
- **COPY**: Copia arquivos do contexto para dentro da imagem;
- **ADD**: Similar ao COPY mas com funcionalidades adicionais (prefira o COPY);

## Instruções comuns

* **WORKDIR**: Altera o diretório atual do build para todos as instruções subsequentes;
* **CMD**: Define o comando final a ser executado quando o container entrar em execução;
* **ENTRYPOINT**: Similar ao CMD, mas utilizado quando a imagem é feita para ser executada como um comando de terminal;

## Instruções comuns

* **ENV**: Seta variáveis de ambiente;
* **LABEL**: Seta dados sobre a imagem (como autor, versão, etc.);
* **EXPOSE**: Determina quais portas de rede serão expostas pela imagem;
* **VOLUME**: Seta volumes utilizados pela imagem.

## Mais um exemplo

```Dockerfile
FROM nginx:latest
COPY . /usr/share/nginx/html
```

## Outro exemplo

```Dockerfile
FROM debian:10

RUN apt-get update && apt-get install -y apache2 && apt-get clean

ENV APACHE_LOCK_DIR="/var/lock"
ENV APACHE_PID_FILE="/var/run/apache2.pid"
ENV APACHE_RUN_USER="www-data"
ENV APACHE_RUN_GROUP="www-data"
ENV APACHE_LOG_DIR="/var/log/apache2"

LABEL description="Webserver"

VOLUME /var/www/html/

EXPOSE 80

ENTRYPOINT ["/usr/sbin/apachectl"]

CMD ["-D", "FOREGROUND"]
```


## Mais Exemplos?

- [Dockerfile da image oficial do Ruby](https://github.com/docker-library/ruby/blob/master/3.1/alpine3.15/Dockerfile).

## Docker Hub (Registry)

- O docker mantém um repositório (registry) oficial de imagens;
- [https://hub.docker.com](https://hub.docker.com/);
- É possível publicar imagens gratuitamente através do hub;
- Basta um cadastro na plataforma;
- [Repositório da imagem oficinal do Ruby](https://hub.docker.com/_/ruby).

## Publicando uma imagem

Login no Docker Hub:

```bash
docker login
```

Ao gerar a imagem, utilize um nome com o seguinte formato:

`seuusuario/nomedaimagem:versão`

## Publicando uma imagem (cont.)

Para finalizar, envie a imagem ao Docker Hub:

```bash
docker push seuusuario/nomedaimagem:versão
```

# Volumes

## Definição

- Um volume é um mecanismo para persistir dados gerados em **containers**. Ou seja, para abrigar novos arquivos gerados em tempo de execução;
- Para entender a importância dos volumes, é necessário compreender a estrutura de uma imagem docker.

## Estrutura de uma imagem docker

![[Fonte](https://www.partech.nl/nl/publicaties/2020/04/docker-series-part-2-of-4-docker-images-and-containers)](./img/multilayer_image.png)

## Copy-On-Write

- Containers compartilham camadas de uma mesma imagem:
  - Todas são *Read Only*;
- Uma nova camada de escrita é criada para cada container;
- **Ao alterar um arquivo** de uma camada inferior da imagem, o **docker copia** esse arquivo para a camada de escrita do container;
- Apenas o container terá acesso a essa nova camada;
- Ao remover um container, sua camada de escrita também é removida.

## Como persistir dados?

- Sabendo que os dados alterados ou criados por um container são perdidos com sua remoção, como **persistir dados**?

. . .

Através de volumes.

## Volumes e Bind mounts

- **Volumes** são uma forma de criar locais de persistência para containers;
  - Podem, inclusive, serem compartilhadas entre diferentes containers.
- **Bind mounts** são locais de persistência compartilhados com o host:
  - Ou seja, arquivos compartilhados entre o computador host e o container em execução.

## Volumes e Bind mounts

![[Fonte](https://docs.docker.com/storage/volumes/)](./img/types-of-mounts-volume.png)

## Exemplo (name volume)

```sh
docker run -d \
  --name devtest \
  -v myvol:/usr/share/nginx/html \
  nginx:latest
```

- Cria um novo container e um volume chamado myvol;
- O volume será montado no diretório `/usr/share/nginx/html` do container.

## Exemplo (cont.)

```sh
docker volume ls
docker volume inspect myvol
```

Comandos para análise dos volumes criados.

## Exemplo (cont.)

```sh
docker container stop devtest
docker container rm devtest
docker run -d --name devtest2 -v myvol:/usr/share/nginx/html nginx:latest
```

Mesmo após remover o container e iniciar outro, os dados continua persistidos.

## Exemplo (bind mount)

```sh
docker run -d --name devtest3 -v /home/ubuntu:/usr/share/nginx/html nginx:latest
```

Se o caminho de origem é de um diretório local, então o diretório do container estará ligado (*bind*) a ele.

## Detalhes importantes

- O volume é inicializado quando o container é criado;
- Caso ocorra já existam dados da imagem no diretório em que você está montando como volume, aqueles dados serão copiados para o volume;
- Um volume pode ser reusado e compartilhado entre containers.

## Detalhes importantes (cont.)

- Alterações em um volume são feitas diretamente no volume;
- Alterações em um volume não irão com a imagem quando você fizer uma cópia ou snapshot de um container;
- Volumes continuam a existir mesmo se você deletar o container.

# Rede

## Publicando portas locais

- Até agora, estamos acessando nossos containers através de seus **ip's privados**;
- Mas e se quisermos expor os serviços de um container para a rede pública?

## Publicando portas locais (cont.)

- Basta, no comando `run`, definir qual porta local deve ser mapeada (publicada) em qual porta do container:

```bash
docker run -d -p 80:80 --name nginx nginx
```

Dessa forma, qualquer acesso na porta 80 do **host** será redirecionado à porta 80 do **container**.

## Comunicação entre containers

- Normalmente, a comunicação entre containers (icc) é desativada por padrão;
- É mais seguro colocar containers que precisam se comunicar em uma rede virtual específica.

## Definindo uma rede virtual

```bash
docker network create ngnet
```

Esse comando cria uma rede virtual chamada ngnet.

## Conectando à rede

Podemos conectar um container já em execução à uma nova rede:

```bash
docker run --name nginx nginx
docker network connect ngnet nginx
```

Ou utilizar o parâmetro ``--net`` no comando `run`:

```bash
docker run --rm -t -i --net ngnet busybox wget http://nginx/
```

## Assunto complexo

- Administração da rede entre containers pode ser bastante complexo no Docker;
- Deixaremos mais detalhes para uma outra disciplina.

# Docker-compose

## Descrição de serviços

- Os comandos vistos até aqui são bastante ágeis para a criação de containers;
- Porém, a execução constante de comandos pode gerar erros;
- Seria muito melhor se pudéssemos descrever em formato de texto os serviços que queremos rodar, não?
- É aí que o `docker-compose` entra em cena.

## Docker-compose

É um programa capaz de ler um arquivo yml com a descrição de vários serviços e os executar corretamente.

## Instalação no Linux

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

## Exemplo

[https://gitlab.com/das-alexkutzke/docker-compose-example](https://gitlab.com/das-alexkutzke/docker-compose-example)

## Comandos comuns

```bash
docker-compose up    # cria e inicia containers
docker-compose up -d # background
docker-compose down  # para e remove containers
docker-compose start # inicia containers sem recriá-los
docker-compose stop  # inicia containers sem removê-los
docker-compose logs  # visualiza logs dos containers
docker-compose pull  # baixa ou atualiza imagens
```

# Multi-stage Dockefile

## Problema

- Docker é uma plataforma para **micro-serviçoes**;
- Portanto, é importante que containers sejam:
  - **Efêmeros**: podem ser parados e iniciados sem grandes problemas (pouco overhead e configuração);
  - **Pequenos**: imagens devem ocupar o mínimo de espaço possível.

## Dockerfile

- Dockerfiles são uma ótima ferramenta para criação de imagens;
- Porém, é muito após o build é comum criarmos imagens que chegam aos Gigabytes de tamanho:
  - Isso não é nem um pouco interessante;
* Uma solução para diminuir tamanho de imagens é utilizar *muilt-stage Dockerfiles*:
  - Quando indicamos que certas camadas poderão ser descartadas durante o build.

## Exemplo

[https://gitlab.com/das-alexkutzke/multi-stage-example](https://gitlab.com/das-alexkutzke/multi-stage-example)

# Referências

DOCKER, "Docs". 2022. Acessado em março de 2022.
[https://docs.docker.com](https://docs.docker.com)

FERNANDO, J. "Descomplicando o Docker". 2018. Acessado em março de 2022.
[https://livro.descomplicandodocker.com.br/](https://livro.descomplicandodocker.com.br/)
